package edu.westga.cs1302.exceptions.controllers;

import java.util.ArrayList;

import edu.westga.cs1302.exceptions.model.Dog;
import edu.westga.cs1302.exceptions.model.ShowDog;
import edu.westga.cs1302.exceptions.model.WorkingDog;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class defines the controller functions for the 
 * 	exceptions application
 * 
 * @author Kathleen Anderson
 * @version 7/11/2021
 */
public class DogController {
	private ArrayList<Dog> theDogs;
	
	/**
	 * Constructor that initializes the object
	 * 
	 * @precondition None
	 * @postcondition The DogController exists
	 */
	public DogController() {
		this.theDogs = new ArrayList<Dog>();
	}
	
	/**
	 * Creates a Show Dog
	 * 
	 * @param name Name of the dog
	 * @param age Age of the dog
	 * @param registrationNumber AKC number of the dog
	 * @precondition name != null; name.length > 0
	 * @precondition age >= 0
	 * @precondition registrationNumber > 0
	 * @postcondition A Show Dog is created with the given values
	 */
	public void createShowDog(String name, String age, String registrationNumber) {
		try {
			this.theDogs.add(new ShowDog(name, Integer.parseInt(age), Integer.parseInt(registrationNumber)));
		} catch (Exception thisException) {
			this.showAlert(thisException);
		}
	}

	/**
	 * Creates a Working Dog
	 * 
	 * @param name Name of the dog
	 * @param age Age of the dog
	 * @param maximumWorkHours Maximum work hours per week for the dog
	 * @precondition name != null; name.length > 0
	 * @precondition age >= 0
	 * @precondition maximumWorkHours >= 0
	 */
	public void createWorkingDog(String name, String age, String maximumWorkHours) {
		try {
			this.theDogs.add(new WorkingDog(name, Integer.parseInt(age), Integer.parseInt(maximumWorkHours)));
		} catch (Exception thisException) {
			this.showAlert(thisException);
		}
	}

	/**
	 * This method will build a String representation of the 
	 * 	dogs in the collection with one dog included
	 * 	per line
	 * 
	 * @return A String holding a description of each dog, with
	 * 			one dog per line
	 */
	public String getDescription() {
		String returnString = "";
		for (Dog current : this.theDogs) {
			returnString += current.toString() + "\n";
		}
		return returnString;
	}
	
	/**
	 * Shows an error message detailing what went wrong
	 * 
	 * @param thisException The exception to be shown
	 * @precondition thisException != null
	 * @postcondition The exception's relevant details are shown to the user
	 */
	public void showAlert(Exception thisException) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Error:");
		
		alert.setContentText(thisException.toString());

		alert.showAndWait();
	}

}
