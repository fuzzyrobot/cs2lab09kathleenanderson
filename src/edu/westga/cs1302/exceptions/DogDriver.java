package edu.westga.cs1302.exceptions;

import edu.westga.cs1302.exceptions.controllers.GuiController;

/**
 * This is the driver class for the exceptions application
 * 
 * @author	CS1302
 * @version	Summer 2021
 *
 */
public class DogDriver {

	/**
	 * Entry-point into the application
	 * 
	 * @param args	Not used
	 */
	public static void main(String[] args) {
		GuiController.show();
	}
}
