package edu.westga.cs1302.exceptions.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.Dog;

class TestCreateDog {
	private Dog myDog;
	
	@BeforeEach
	void setUp() throws Exception {
		this.myDog = new Dog("Lola", 7);
	}
	
	@Test
	void testToString() {
		assertEquals("Lola, 7 years old", this.myDog.toString());
	}

	@Test
	void testGetName() {
		assertEquals("Lola", this.myDog.getName());
	}
	
	@Test
	void testGetAge() {
		assertEquals(7, this.myDog.getAge());
	}
}
