package edu.westga.cs1302.exceptions.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.ShowDog;

class TestCreateShowDog {
	private ShowDog myDog;
	
	@BeforeEach
	void setUp() throws Exception {
		this.myDog = new ShowDog("Babette", 12, 1223344);
	}
	
	@Test
	void testGetName() {
		assertEquals("Babette", this.myDog.getName());
	}
	
	@Test
	void testGetAge() {
		assertEquals(12, this.myDog.getAge());
	}

	@Test
	void testToString() {
		assertEquals("Babette, 12 years old, AKC Registration #1223344", this.myDog.toString());
	}

	@Test
	void testGetRegistration() {
		assertEquals(1223344, this.myDog.getRegistration());
	}

}
