package edu.westga.cs1302.exceptions.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.westga.cs1302.exceptions.model.WorkingDog;

class TestCreateWorkingDog {
	private WorkingDog myDog;
	
	@BeforeEach
	void setUp() throws Exception {
		this.myDog = new WorkingDog("Star", 6, 21);
	}
	
	@Test
	void testGetName() {
		assertEquals("Star", this.myDog.getName());
	}
	
	@Test
	void testGetAge() {
		assertEquals(6, this.myDog.getAge());
	}
	
	@Test
	void testToString() {
		assertEquals("Star, 6 years old, can work a maximum of 21 hours per week",
				this.myDog.toString());
	}
	
	@Test
	void testGetHours() {
		assertEquals(21, this.myDog.getWorkHours());
	}
}
