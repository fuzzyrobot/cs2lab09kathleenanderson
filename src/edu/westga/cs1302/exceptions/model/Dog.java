package edu.westga.cs1302.exceptions.model;

/**
 * Class that models a Dog object
 * 
 * @author Kathleen Anderson
 * @version 7/11/2021
 */
public class Dog {
	private int age;
	private String name;
	
	/**
	 * Constructs a Dog object with the given age
	 * 
	 * @param newName Name of the dog
	 * @param newAge Age of the dog
	 * @precondition newName != null; newName.length() > 0
	 * @precondition newAge >= 0
	 * @postcondition A new Dog object exists with the given values
	 */
	public Dog(String newName, int newAge) {
		if (newName == null || newName.length() == 0) {
			throw new IllegalArgumentException("Must input a valid name");
		}
		if (newAge < 0) {
			throw new IllegalArgumentException("Age cannot be negative");
		}
		this.name = newName;
		this.age = newAge;
	}
	
	/**
	 * Returns the name of the dog
	 * 
	 * @precondition None
	 * @return The dog's name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Returns the age of the dog
	 * 
	 * @precondition none
	 * @return The dog's age
	 */
	public int getAge() {
		return this.age;
	}
	
	/**
	 * Returns a string describing the dog
	 * 
	 * @precondition None
	 * @return A string describing the dog
	 */
	public String toString() {
		return this.name + ", " + this.age + " years old";
	}
}
