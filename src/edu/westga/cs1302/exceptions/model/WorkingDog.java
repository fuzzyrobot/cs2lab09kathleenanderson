package edu.westga.cs1302.exceptions.model;

/**
 * Models an object that represents a Working Dog
 * 
 * @author Kathleen Anderson
 * @version 7/11/2021
 */
public class WorkingDog extends Dog {
	private int workHours;
	
	/**
	 * Creates a Working Dog with a maximum amount of work hours per week
	 * 
	 * @param newName The dog's name
	 * @param newAge The dog's age
	 * @param newWorkHours Maximum hours the dog can work a week
	 * @precondition newName != null && newName.length > 0
	 * @precondition newAge >= 0
	 * @precondition newWorkHours >= 0
	 * @postcondition A new Working Dog exists with the given values
	 */
	public WorkingDog(String newName, int newAge, int newWorkHours) {
		super(newName, newAge);
		if (newWorkHours < 0) {
			throw new IllegalArgumentException("Work hours cannot be negative");
		}
		this.workHours = newWorkHours;
	}
	
	/**
	 * Returns the maximum number of hours the dog can work in a week
	 * 
	 * @precondition none
	 * @return The number of hours a week the dog can work
	 */
	public int getWorkHours() {
		return this.workHours;
	}
	
	/**
	 * Returns a representation of the object as a string
	 * 
	 * @precondition None
	 * @return The object represented by a string
	 */
	public String toString() {
		return super.toString() + ", can work a maximum of " + this.workHours
				+ " hours per week";
	}
}
