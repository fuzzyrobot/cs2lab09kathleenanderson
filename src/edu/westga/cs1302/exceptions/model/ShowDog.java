package edu.westga.cs1302.exceptions.model;

/**
 * Creates an object modeling a show dog
 * 
 * @author Kathleen Anderson
 * @version 7/11/2021
 */
public class ShowDog extends Dog {
	private int registration;
	
	/**
	 * Creates a Show Dog with the given values
	 * 
	 * @param newName Name of dog
	 * @param newAge Age of dog
	 * @param newRegistration AKC registration number
	 * @precondition newName != null && newName.length > 0
	 * @precondition newAge >= 0
	 * @precondition newRegistration >= 0
	 * @postcondition A new Show Dog exists with the given values
	 */
	public ShowDog(String newName, int newAge, int newRegistration) {
		super(newName, newAge);
		if (newRegistration < 0) {
			throw new IllegalArgumentException("Registration number cannot be negative");
		}
		this.registration = newRegistration;
	}
	
	/**
	 * Returns the AKC registration number
	 * 
	 * @precondition none
	 * @return The AKC registration number
	 */
	public int getRegistration() {
		return this.registration;
	}
	
	/**
	 * Returns a string describing the show dog
	 * 
	 * @precondition none
	 * @return A string describing the object
	 */
	public String toString() {
		return super.toString() + ", AKC Registration #" + this.registration;
	}
}